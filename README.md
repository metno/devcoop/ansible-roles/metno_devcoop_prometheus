# Description

Ansible role to install [prometheus](https://prometheus.io/) and alertmanager on a host.

# Prometheus configuration

To add scrape targets from hosts known to ansible, make
`prometheus_scrape_ansible_hosts` a list of maps where `targets`
contains an ansible group, something like:

    prometheus_scrape_ansible_hosts:
      - job_name: "workstation"
        targets: "{{ groups.workstations }}"
        scheme: http           # optional
        metrics_path: /metrics # optional
        port: 9100             # optional
      - job_name: "data"
        targets: "{{ groups.data_servers }}"

For more complicated configurations, set `prometheus_scrape_configs`
to the content (a list) of
[scrape_configs](https://prometheus.io/docs/prometheus/latest/configuration/configuration/#scrape_config)
(see also https://prometheus.io/docs/guides/file-sd/).
